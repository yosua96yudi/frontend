import { Form, InputGroup } from "react-bootstrap";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import axios from "axios";
import { API_AUTH } from "../../utils/BaseUrl";

function EditObat() {
    const [namaObat, setNamaObat] = useState();
    const [dosis, setDosis] = useState();

    const navigate = useNavigate();
    const param = useParams();

    const namaObatChangeHandler = (event) => {
        setNamaObat(event.target.value);
    }

    const dosisChangeHandler = (event) => {
        setDosis(event.target.value);
    }

    const submitActionHandler = async (event) => {
        event.preventDefault();

        await axios.put(`${API_AUTH}/api/daftar-obat/` + param.id, {
            namaObat: namaObat,
            dosis: dosis
        }, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        }).then(() => {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Edit Success!!',
                showConfirmButton: false,
                timer: 1500
            });
            navigate("/daftar-obat")
        }).catch(error => {
            alert("Terjadi kesalahan: " + error);
        })
    };

    const cancel = () => {
        navigate("/daftar-obat");
    }

    useEffect(() => {
        axios.get(`${API_AUTH}/api/daftar-obat/` + param.id, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        }).then((response) => {
            const { namaObat, dosis } = response.data;
            setNamaObat(namaObat);
            setDosis(dosis);
        }).catch(error => {
            alert("Terjadi kesalahan Sir! " + error);
        });
    }, []);

    return (
        <div className="">
            <div className="flex justify-content-center container mx-auto pt-6 ml-4 mr-4">
                <div className="w-1/2 px-1 py-5 bg-white rounded-lg shadow">
                    <Form onSubmit={submitActionHandler} className="ml-4 mr-4">
                        <div className="mb-4 text-center">
                            <h1>Edit Obat</h1>
                        </div>
                        <div className="name mb-3">
                            <Form.Label>
                                <strong>Nama Obat</strong>
                            </Form.Label>
                            <InputGroup className="d-flex gap-3">
                                <Form.Control placeholder="Nama Obat" value={namaObat} onChange={namaObatChangeHandler} />
                            </InputGroup>
                        </div>
                        <div className="name mb-3">
                            <Form.Label>
                                <strong>Stock Obat</strong>
                            </Form.Label>
                            <InputGroup className="d-flex gap-3">
                                <Form.Control placeholder="Dosis Obat" value={dosis} onChange={dosisChangeHandler} />
                            </InputGroup>
                        </div>
                        <div className="d-flex justify-content-end align-items-center mt-2">
                            <button className="bg-red-600
                            text-white hover:bg-red-700 
                            focus:ring-4 focus:outline-none focus:ring-red-300 
                            font-medium rounded-lg text-md w-full sm:w-auto px-4 py-2 text-center" onClick={cancel}>
                                Batal
                            </button>
                            <div className="ms-2 me-2">||</div>
                            <button className="bg-green-500
                            text-white hover:bg-green-600 
                            focus:ring-4 focus:outline-none focus:ring-green-300 
                            font-medium rounded-lg text-md w-full sm:w-auto px-4 py-2 text-center" type="submit">Simpan</button>
                        </div>
                    </Form>
                </div>
            </div>
        </div >
    )
}

export default EditObat;